import ExampleComponent from './components/ExampleComponent.vue';
import {createApp} from 'vue';

import.meta.glob([
    '../img/**',
]);

const app = createApp({});

app
    .component('example-component', ExampleComponent)
    .mount('#app')
