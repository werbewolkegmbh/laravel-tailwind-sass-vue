
# Example Projekt für Laravel und Vite mit Vue und Tailwind vorinstalliert

## Allgemeine Infos
- Laravel
- Vite

## Frameworks
- Tailwind
- Vue

## Versionen
- node: 20.x.x
- php 8.2

## Lokal aufsetzten
- `composer install` composer package installieren
- `.env` Datei anlegen (zB mit `cp .env.example .env`)
- `php artisan key:generate` im Root Ordner ausführen
- `cd public/ && valet link example && cd ..` valet link (example) in den public Ordner setzen, hier individuell anpassen
- Den angelegt Link in der `.env` bei APP_URL angeben
- `npm ci` npm installieren
- `npm run dev` watch starten

# Wichtig:
- mit `git describe --tags $(git rev-list --tags --max-count=1)` herrausfinden welche Version aktuell ist
- dann Version um +0.0.1 anheben mit `git tag vx.x.x` und `git push origin vx.x.x` dann den geänderten Tag pushen
- wenn das nicht gemacht wird, werden die änderungen auf Packagist nicht übernommen!

## Production
`npm run build`

## Quelldaten
`resources`
