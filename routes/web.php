<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});
Route::get('/ueber-uns', function () {
    return view('about');
});
Route::get('/kontakt', function () {
    return view('contact');
});
Route::get('/impressum', function () {
    return view('impressum');
});
Route::get('/datenschutz', function () {
    return view('datenschutz');
});
