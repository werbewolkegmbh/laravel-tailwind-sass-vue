/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    theme: {
        fontFamily: {
            'sans': ["Roboto Slap"],
        },
        screens: {
            '2xs': '320px',
            'xs': '400px',
            'sm': '600px',
            'md': '768px',
            'lg': '1024px',
            'xl': '1200px',
            '2xl': '1400px',
            '3xl': '1700px',
            '4xl': '1900px',

            'sm-only': {'max': '767px'},
            'md-only': {'min': '768px', 'max': '1023px'},
            'lg-only': {'min': '1024px', 'max': '1259px'},
            'xl-only': {'min': '1260px', 'max': '1920px'},
        },
        colors: {
            white: '#ffffff',
            black: '#000000',
            green: '#346F35',
            blue: '#27316C',
            red: '#DA332D',
        },
        spacing: generateSpacing(0, 500),
        extend: {
            borderWidth: {
                1: "1px"
            },
            gridColumnEnd: {
                '13': '13',
                '14': '14',
                '15': '15',
            },
            backgroundImage: {
                'green-gradient': 'linear-gradient(to bottom, #007A33, #C4D600)',
            },
        },
    },
    plugins: [],
}

function generateSpacing(min, max) {
    const spacing = {};
    for (let i = min; i <= max; i++) {
        spacing[i] = `${i}px`;
    }
    return spacing;
}
